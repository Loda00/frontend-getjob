import axios from 'axios'

/* RENZO CAMBIA SOLO https://1e9799bd.ngrok.io el /Get_Job dejalo como está */
axios.defaults.baseURL = 'https://1e9799bd.ngrok.io/Get_Job'; /* < === */
/*  ESTE ES EL QUE TIENES QUE CAMBIAR  PORFA     */


// axios.
// axios.defaults.headers.common = {
//   Authorization: `Bearer ${JSON.parse(sessionStorage.getItem('jwt'))}`,
// }

export const httpGet = async (page, params) => {
  const service = params ? `${page}/${params}` : page
  try {
    const response = await axios.get(service, {
      params,
    })
    // console.log('response', response)
    return response.data
  } catch (error) {
    throw new Error(error)
  }
}

export const httpPost = async (page, params) => {
  try {
    const response = await axios.post(page, params)
    // console.log('response', response)
    return response.data
  } catch (error) {
    throw new Error(error)
  }
}

export const httpPut = async (page, params) => {
  try {
    const response = await axios.put(page, params)
    // console.log('response', response)
    return response.data
  } catch (error) {
    throw new Error(error)
  }
}

export const httpDelete = async (page, params) => {
  try {
    const response = await axios.delete(page, params)
    // console.log('response', response)
    return response.data
  } catch (error) {
    throw new Error(error)
  }
}

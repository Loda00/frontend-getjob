import { combineReducers } from 'redux'

import users from './users/index'
import distrito from './distrito'
import auth from './auth/index'
import trabajo from './trabajo'
import area from './area'
import postulacion from './postulaciones'
import centroEstudios from './centroEstudios'
import pais from './pais'
import departamento from './departamento'
import provincia from './provincia'
import rol from './rol'

const reducers = combineReducers({
  users,
  distrito,
  auth,
  trabajo,
  area,
  postulacion,
  centroEstudios,
  pais,
  departamento,
  provincia,
  rol,
})

// const rootReducer = (state, action) => {
//   // switch (action.type) {
//   if (action.type === 'LOG_OUT') {
//     console.log('xxxxxxxxxx', reducers, 'actionaction', action)
//     localStorage.setItem('GetJob', {});
//     return {
//       ...reducers,
//       auth: {
//         created: null,
//         error: null,
//         loading: false,
//       },
//     }
//   }
// }

export default reducers

import React from 'react';
import FacebookLogin from 'react-facebook-login'
// import GoogleLogin from 'react-google-login'
import { connect } from 'react-redux'
import { Icon } from 'semantic-ui-react'
import { create } from 'actions/users/create'
// import { user as dataUser } from '../../actions/user/user'
// import { tokens } from '../../config/config'

class LoginRedSocial extends React.Component {
  state = {
    // isLogin: false,
    // user: [],
    tokenFb: 1060735587445697,
    tokenGoogle: '367476223355-itoafav8at7mhstscn0uehhfg0d960l1.apps.googleusercontent.com',
  }

  login = (response) => {
    // const { user } = this.state

    const { createUser } = this.props
    console.log('response', response)
    if (response && (response.El || response.userID)) {
      const user = response.userID ? response : response.profileObj
      const data = {
        id: user.userID ? user.userID : user.googleId,
        // contrasenia: ""
        // name: user.name,
        // correo: user.email,
        photo: user.userID ? user.picture.data.url : user.imageUrl,
        contrasenia: '',
        nombres: user.name,
        correo: user.email,
        numero: '',
        // apellidos,
        // idEstado,
        // idPais,
        // idDepartamento,
        // idProvincia,
        // idDistrito,
        // idCentroEstudios,
        // idRol,
        // presentacion,
      }
      console.log('data', data)
      createUser(createUser)
    }
  }

  errorLogin = (response) => {
    console.log(response);
  }

  render() {
    const { tokenGoogle, tokenFb } = this.state
    console.log('tokenGoogle', tokenGoogle)
    return (
      <div className="buton-social-auth">
        {/* <GoogleLogin
          className="btn-social"
          clientId={tokenGoogle}
          buttonText="Continuar con Google"
          onSuccess={this.login}
          onFailure={this.errorLogin}
        /> */}
        <FacebookLogin
          className="btn-social"
          appId={tokenFb}
          fields="name,email,picture"
          cssClass="button-auth-fb"
          textButton="Continuar con Facebook"
          onFailure={this.errorLogin}
          callback={this.login}
          icon={<Icon name="facebook" />}
        />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  createUser: obj => dispatch(create(obj)),
})

export default connect(null, mapDispatchToProps)(LoginRedSocial)

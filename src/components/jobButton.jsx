import React from 'react'
import { Button, Icon } from 'semantic-ui-react'

const JobButton = ({ icon, children, onClick, loading, style }) => (
  <div className="job-button" style={style}>
    <Button
      color="facebook"
      size="mini"
      onClick={onClick}
      loading={loading}
    >
      <Icon name={icon} />
      {children}
    </Button>
  </div>
)

export default JobButton

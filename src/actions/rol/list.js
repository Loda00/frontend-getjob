import { Toast } from 'components';
import { MESSAGE_ERROR } from 'const'
import { httpGet } from 'utils'

export function error(error) {
  return {
    type: 'ROL_LIST_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'ROL_LIST_LOADING', loading,
  };
}

export function success(data) {
  return {
    type: 'ROL_LIST_SUCCESS', data,
  };
}

export function list(id, page = '/rol/listarRoles') {
  return (dispatch) => {
    dispatch(loading(true));
    dispatch(error(''));
    httpGet(page, id)
      .then((data) => {
        console.log('datadatadata', data)
        dispatch(loading(false));
        dispatch(success(data));
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        console.log('ERRRRR')
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

export function reset() {
  return {
    type: 'ROL_LIST_RESET',
  };
}

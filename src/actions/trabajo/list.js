import { Toast } from 'components';
import { MESSAGE_ERROR } from 'const'
import { httpGet } from 'utils/index'

export function error(error) {
  return {
    type: 'TRABAJO_LIST_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'TRABAJO_LIST_LOADING', loading,
  };
}

export function success(data) {
  return {
    type: 'TRABAJO_LIST_SUCCESS', data,
  };
}

export function list(id, page = '/trabajo/listarTrabajos') {
  return (dispatch) => {
    dispatch(loading(true));
    dispatch(error(''));
    httpGet(page, id)
      .then((data) => {
        console.log('datadatadata', data)
        dispatch(loading(false));
        dispatch(success(data));
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        console.log('ERRRRR')
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

export function reset() {
  return {
    type: 'TRABAJO_LIST_RESET',
  };
}

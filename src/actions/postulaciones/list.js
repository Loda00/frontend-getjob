import { Toast } from 'components';
import { MESSAGE_ERROR } from 'const'
import { httpPost } from 'utils'

export function error(error) {
  return {
    type: 'POSTULACIONES_LIST_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'POSTULACIONES_LIST_LOADING', loading,
  };
}

export function success(data) {
  return {
    type: 'POSTULACIONES_LIST_SUCCESS', data,
  };
}

export function list(obj, page = '/detalleTrabajo/listarDetalleTrabajos') {
  return (dispatch) => {
    dispatch(loading(true));
    dispatch(error(''));
    httpPost(page, obj)
      .then((data) => {
        dispatch(loading(false));
        dispatch(success(data));
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

export function reset() {
  return {
    type: 'POSTULACIONES_LIST_RESET',
  };
}

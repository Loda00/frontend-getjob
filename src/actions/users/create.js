import { Toast } from 'components';
import { MESSAGE_ERROR, MESSAGE_SUCCESS } from 'const'
import { httpPost } from 'utils/index'

export function error(error) {
  console.log('USUARIO_CREATE_ERROR', error)
  return {
    type: 'USUARIO_CREATE_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'USUARIO_CREATE_LOADING', loading,
  };
}

export function success(created) {
  console.log('USUARIO_CREATE_SUCCESS', created)
  return {
    type: 'USUARIO_CREATE_SUCCESS', created,
  };
}

export function create(obj, page = '/usuario/crearUsuarioRapido') {
  return (dispatch) => {
    dispatch(loading(true));
    dispatch(error(null))

    httpPost(page, obj)
      .then((data) => {
        dispatch(loading(false));
        dispatch(success(data));
        Toast.success(MESSAGE_SUCCESS);
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

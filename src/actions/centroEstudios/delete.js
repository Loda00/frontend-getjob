import { Toast } from 'components';
import { MESSAGE_ERROR, MESSAGE_SUCCESS } from 'const'
import { httpDelete } from 'utils'

export function error(error) {
  return {
    type: 'CENTRO_ESTUDIOS_DELETE_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'CENTRO_ESTUDIOS_DELETE_LOADING', loading,
  };
}

export function success(deleted) {
  return {
    type: 'CENTRO_ESTUDIOS_DELETE_SUCCESS', deleted,
  };
}

export function del(obj) {
  return (dispatch) => {
    dispatch(loading(true));

    httpDelete(obj)
      .then((rs) => {
        dispatch(loading(false));
        dispatch(success(rs.data));
        Toast.success(MESSAGE_SUCCESS);
      })
      .catch((e) => {
        dispatch(loading(false));
        Toast.error(MESSAGE_ERROR);
        dispatch(error(e.message));
      });
  };
}

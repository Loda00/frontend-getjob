import { Toast } from 'components';
import { MESSAGE_ERROR } from 'const'
import { httpGet } from 'utils'

export function error(error) {
  return {
    type: 'CENTRO_ESTUDIOS_LIST_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'CENTRO_ESTUDIOS_LIST_LOADING', loading,
  };
}

export function success(data) {
  return {
    type: 'CENTRO_ESTUDIOS_LIST_SUCCESS', data,
  };
}

export function list(id, page = '/centroDeEstudio/listarCentroDeEstudios') {
  return (dispatch) => {
    dispatch(loading(true));
    dispatch(error(''));
    httpGet(page, id)
      .then((data) => {
        dispatch(loading(false));
        dispatch(success(data));
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

export function reset() {
  return {
    type: 'CENTRO_ESTUDIOS_LIST_RESET',
  };
}

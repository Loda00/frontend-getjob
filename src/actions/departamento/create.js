import { Toast } from 'components';
import { MESSAGE_ERROR, MESSAGE_SUCCESS } from 'const'
import { httpPost } from 'utils'

export function error(error) {
  return {
    type: 'DEPARTAMENTO_CREATE_ERROR', error,
  };
}

export function loading(loading) {
  return {
    type: 'DEPARTAMENTO_CREATE_LOADING', loading,
  };
}

export function success(created) {
  console.log('DEPARTAMENTO_CREATE_SUCCESS', created)
  return {
    type: 'DEPARTAMENTO_CREATE_SUCCESS', created,
  };
}

export function create(obj, page = '/trabajo/crearTrabajo') {
  return (dispatch) => {
    dispatch(loading(true));

    httpPost(page, obj)
      .then((data) => {
        dispatch(loading(false));
        dispatch(success(data));
        Toast.success(MESSAGE_SUCCESS);
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        dispatch(loading(false));
        dispatch(error(e.message));
      });
  };
}

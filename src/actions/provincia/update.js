import { Toast } from 'components';
import { MESSAGE_ERROR, MESSAGE_SUCCESS } from 'const'
import { httpPut } from 'utils'
import { success as createSuccess } from './create';

export function retrieveError(retrieveError) {
  return {
    type: 'PROVINCIA_UPDATE_RETRIEVE_ERROR', retrieveError,
  };
}

export function retrieveLoading(retrieveLoading) {
  return {
    type: 'PROVINCIA_UPDATE_RETRIEVE_LOADING', retrieveLoading,
  };
}

export function retrieveSuccess(retrieved) {
  return {
    type: 'PROVINCIA_UPDATE_RETRIEVE_SUCCESS', retrieved,
  };
}

export function retrieve(id, page = '') {
  return (dispatch) => {
    dispatch(retrieveLoading(true));

    httpPut(id, page)
      .then((data) => {
        dispatch(retrieveLoading(false));
        dispatch(retrieveSuccess(data));
      })
      .catch((e) => {
        Toast.error(MESSAGE_ERROR);
        dispatch(retrieveLoading(false));
        dispatch(retrieveError(e.message));
      });
  };
}

export function updateError(updateError) {
  return {
    type: 'PROVINCIA_UPDATE_UPDATE_ERROR', updateError,
  };
}

export function updateLoading(updateLoading) {
  return {
    type: 'PROVINCIA_UPDATE_UPDATE_LOADING', updateLoading,
  };
}

export function updateSuccess(updated) {
  return {
    type: 'PROVINCIA_UPDATE_UPDATE_SUCCESS', updated,
  };
}

export function update(obj, page = '') {
  return (dispatch) => {
    dispatch(updateError(null));
    dispatch(createSuccess(null));
    dispatch(updateLoading(true));

    httpPut(obj, page)
      .then((data) => {
        Toast.success(MESSAGE_SUCCESS);
        dispatch(updateLoading(false));
        dispatch(updateSuccess(data));
      })
      .catch((e) => {
        dispatch(updateLoading(false));
        Toast.error(MESSAGE_ERROR);
        dispatch(updateError(e.message));
      });
  };
}

export function reset() {
  return {
    type: 'PROVINCIA_UPDATE_RESET',
  };
}

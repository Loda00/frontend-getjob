export const MESSAGE_ERROR_LOGIN = 'Usuario o contraseña incorrecta'

export const MESSAGE_SUCCESS_LOGIN = 'Bienvenido'

export const MESSAGE_ERROR = 'Ocurrió un error'

export const MESSAGE_SUCCESS = 'Se guardó correctamente'

export const ROLES = {
  SUDO: '1',
  EMPRESA: '2',
  POSTULANTE: '3',
}

export const ESTADOS = {
  ACTIVO: '1',
  INACTICO: '2',
}

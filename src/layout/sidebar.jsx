import React, { Component, Fragment } from 'react'
import { Image, Icon } from 'semantic-ui-react'
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Collapse } from 'antd'

import Logo from 'image/logo4.png'
import PhotoUser from 'image/perfil.png'

class Sidebar extends Component {
  componentDidMount() {

  }

  panelAcorden = (item, title) => (
    <label className="title-acordeon"><Icon name={item} /> {title}</label>
  )

  handleRedirectHome = () => {
    const { history } = this.props
    history.push('/job')
  }

  render() {
    const {
      user: {
        nombres,
        apellidos,
        idRol,
      },
    } = this.props

    return (
      <Fragment>
        <div className="sidebar">
          <div className="sidebar-fondo">
            <span
              className="sidebar-logo"
              onClick={this.handleRedirectHome}
            >
              <Image
                src={Logo}
                size="tiny"
                style={{
                  width: '230px',
                  height: '70px',
                  cursor: 'pointer',
                }}
              />
            </span>
            <div className="sidebar-content">
              <div className="sidebar-user">
                <span className="photo-user">
                  <Image
                    src={PhotoUser}
                    size="tiny"
                    style={{
                      width: '100%',
                      borderRadius: '50%',
                      cursor: 'pointer',
                    }}
                  />
                </span>
                <div>{nombres}</div>
                <div>{apellidos}</div>
              </div>
              <span>
                Principal
              </span>
              <div className="sidebar-routes">
                <div className="sidebar-acordeon">
                  <Collapse accordion expandIconPosition="right">
                    <Collapse.Panel header={this.panelAcorden('plus', 'Empleos')} key="1">
                      <NavLink
                        to="/job"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Empleos
                        </div>
                      </NavLink>
                      {idRol !== 3 && (
                        <NavLink
                          to="/job/create"
                          className="sidebar-sub-menu-item"
                        >
                          <div className="sidebar-sub-menu-title">
                            Crear empleo
                          </div>
                        </NavLink>
                      )}
                      <NavLink
                        to="/HDB/buscar-usuario"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Ocurrencias
                        </div>
                      </NavLink>
                    </Collapse.Panel>

                    <Collapse.Panel header={this.panelAcorden('edit', 'Postulaciones')} key="2">
                      <NavLink
                        to="/postulacion/mis-postulaciones"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Mis postulaciones
                        </div>
                      </NavLink>
                      <NavLink
                        to="/postulacion/"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Empleos
                        </div>
                      </NavLink>
                    </Collapse.Panel>

                    {/* <Collapse.Panel header={this.panelAcorden('search', 'Buscar HDB')} key="3">
                      <NavLink
                        to="/HDB/buscar-usuario"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Buscar hoja de datos
                        </div>
                      </NavLink>
                    </Collapse.Panel>
                    <Collapse.Panel header={this.panelAcorden('home', 'PC 15 de la UU')} key="4">
                      <NavLink
                        to="/pc/nuevo-pc"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Nuevo PC 15
                        </div>
                      </NavLink>
                      <NavLink
                        to="/pc/actualizar-pc"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Actualizar PC 15
                        </div>
                      </NavLink>
                    </Collapse.Panel>
                    <Collapse.Panel header={this.panelAcorden('home', 'Importar HDB ')} key="5">
                      <NavLink
                        to="/importar/buscar-usuario"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          HDB otra dependencia
                        </div>
                      </NavLink>
                    </Collapse.Panel>
                    <Collapse.Panel header={this.panelAcorden('home', 'Exportar')} key="6">
                      <NavLink
                        to="/exportar/HDB"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Exportar HDB
                        </div>
                      </NavLink>
                    </Collapse.Panel>
                    <Collapse.Panel header={this.panelAcorden('home', 'Ocurrencia')} key="7">
                      <NavLink
                        to="/HDB/buscar-usuario"
                        className="sidebar-sub-menu-item"
                      >
                        <div className="sidebar-sub-menu-title">
                          Ocurrencias generales
                        </div>
                      </NavLink>
                    </Collapse.Panel> */}
                  </Collapse>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.show.retrieved,
})

const Main = connect(mapStateToProps)(Sidebar)

export default withRouter(Main)

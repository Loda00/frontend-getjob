import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Icon, Input, TransitionablePortal, Segment, Header } from 'semantic-ui-react'
import { withRouter, NavLink } from 'react-router-dom'
// import { stateSidebar } from 'actions/sidebar'
import { retrieved } from 'actions/auth/auth'
// import { logout } from 'actions/auth/auth'

import Footer from './footer'

class Body extends Component {
  state = {
    item1: false,
    item2: false,
    item3: false,
    item4: false,
    item5: false,
  }

  componentDidMount() {

  }

  handleClickShow = (name) => {
    this.setState(prevState => (
      {
        [name]: !prevState[name],
      }
    ))
  }

  handleClickClose = (name) => {
    this.setState({
      [name]: false,
    })
  }

  handleExit = () => {
    const { exit, history } = this.props
    exit(null)
    history.push('/login')
  }

  render() {
    const { item4 } = this.state
    const { children } = this.props
    return (
      <Fragment>
        <div className="body">
          <div className="head-body">
            <div className="head-body-left">
              <Input
                loading
                icon="user"
                placeholder="Search..."
                className="input-search"
              />
            </div>
            <div className="head-body-right">
              {/* <span onClick={() => this.handleClickShow('item1')}>
                <Icon name="home" className="head-items" />&nbsp;
                <TransitionablePortal
                  onClose={() => this.handleClickClose('item1')}
                  transition={{
                    animation: 'fade down',
                    duration: 500,
                  }}
                  open={item1}
                >
                  <Segment className="header-items-show">
                    <Header>This is a controlled portal</Header>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                  </Segment>
                </TransitionablePortal>
              </span>
              <span onClick={() => this.handleClickShow('item2')}>
                <Icon name="comment alternate outline" className="head-items" />&nbsp;
                <TransitionablePortal
                  onClose={() => this.handleClickClose('item2')}
                  transition={{
                    animation: 'fade down',
                    duration: 500,
                  }}
                  open={item2}
                >
                  <Segment className="header-items-show">
                    <Header>This is a controlled portal</Header>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                  </Segment>
                </TransitionablePortal>
              </span>
              <span onClick={() => this.handleClickShow('item3')}>
                <Icon name="bell outline" className="head-items" />&nbsp;
                <TransitionablePortal
                  onClose={() => this.handleClickClose('item3')}
                  transition={{
                    animation: 'fade down',
                    duration: 500,
                  }}
                  open={item3}
                >
                  <Segment className="header-items-show">
                    <Header>This is a controlled portal</Header>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                  </Segment>
                </TransitionablePortal>
              </span> */}
              <span onClick={() => this.handleClickShow('item4')}>
                <Icon name="settings" className="head-items" />&nbsp;
                <TransitionablePortal
                  onClose={() => this.handleClickClose('item4')}
                  transition={{
                    animation: 'fade down',
                    duration: 500,
                  }}
                  open={item4}
                >
                  <Segment className="header-items-show">
                    <Header>This is a controlled portal</Header>
                    <div className="efect-options">
                      <NavLink
                        to="/perfil/editar"
                      >
                        Perfil
                      </NavLink>
                    </div>
                    <div className="efect-options">Otros</div>
                    <div className="efect-options" onClick={this.handleExit}>Exit</div>
                  </Segment>
                </TransitionablePortal>
              </span>
              {/* <span onClick={() => this.handleClickShow('item5')}>
                <Icon name="question" className="head-items" />&nbsp;
                <TransitionablePortal
                  onClose={() => this.handleClickClose('item5')}
                  transition={{
                    animation: 'fade down',
                    duration: 500,
                  }}
                  open={item5}
                >
                  <Segment className="header-items-show">
                    <Header>This is a controlled portal</Header>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                    <div className="efect-options">Item Option</div>
                  </Segment>
                </TransitionablePortal>
              </span> */}
            </div>
          </div>
          <div className="body-content">
            {children}
          </div>
        </div>
        <Footer />
      </Fragment>
    )
  }
}
const mapStateToProps = state => ({
  side: state,
})

const mapDispatchToProps = dispatch => ({
  // setStateSide: value => dispatch(stateSidebar(value)),
  exit: data => dispatch(retrieved(data)),
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Body)

export default withRouter(Main)

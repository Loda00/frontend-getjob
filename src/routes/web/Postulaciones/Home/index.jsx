import React, { Fragment, Component } from 'react'
import { Card, Header, Segment, Dimmer, Loader, Button, Icon } from 'semantic-ui-react'
import { Empty } from 'antd'
import { isEmpty } from 'lodash'
import { list as getPostulaciones, reset as resetPostulaciones } from 'actions/postulaciones/list'
import { connect } from 'react-redux'

class Home extends Component {
  state = {
    listPostulaciones: null,
  }

  componentDidMount() {
    const { getPostulaciones, user } = this.props

    const obj = {
      id: user.id,
    }

    getPostulaciones(obj)
  }

  static getDerivedStateFromProps(prevProps, prevState) {
    if (!isEmpty(prevProps.listPostulaciones)
    && prevProps.listPostulaciones !== prevState.listPostulaciones) {
      return {
        listPostulaciones: prevProps.listPostulaciones,
      }
    }

    return null
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    const { listPostulaciones } = this.state

    let optionPostulaciones = []
    if (!isEmpty(listPostulaciones)) {
      optionPostulaciones = listPostulaciones.map(({ titulo, descripcion }, index) => (
        <div key={index} className="lista-trabajos">
          <div className="trabajo-header">
            <div className="trabajo-titulo">{titulo}</div>
            <Button
              className="trabajo-postulado"
              disabled
              loading={false}
              size="mini"
            >
              <Icon name="briefcase" />Postulado
            </Button>
          </div>
          <div className="trabajo-descripcion">
            {`- ${descripcion}`}
          </div>
        </div>
      ))
    }

    return (
      <Fragment>
        <div className="content-view">
          <Header className="content-title" as="h2">Mis postulaciones</Header>
          <Segment
            basic
          >
            <Dimmer active={false} inverted>
              <Loader>Loading ...</Loader>
            </Dimmer>
            <Card className="card-content-custom">
              <Card.Content>
                {!isEmpty(optionPostulaciones)
                  ? optionPostulaciones
                  : (
                    <Empty
                      style={{
                        marginTop: '100px',
                      }}
                    />
                  )}
              </Card.Content>
            </Card>
          </Segment>
          <div className="back" />
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  listPostulaciones: state.postulacion.list.data,
  user: state.auth.show.retrieved,
})

const mapDispatchToProps = dispatch => ({
  getPostulaciones: id => dispatch(getPostulaciones(id)),
  reset: () => {
    dispatch(resetPostulaciones())
  },
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Home)

export default Main

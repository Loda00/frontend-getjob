import React, { Fragment, Component } from 'react'
import { Card, Header, Segment, Dimmer, Loader, Icon, Button } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import { isEmpty } from 'lodash'
import { list as getTrabajo, reset as resetTrabajo } from 'actions/trabajo/list'
import { list as getPostulaciones, reset as resetPostulaciones } from 'actions/postulaciones/list'
import { create as postPostulacion } from 'actions/postulaciones/create'
import { success as successTrabajo } from 'actions/trabajo/create'
import { connect } from 'react-redux'

class Home extends Component {
  state = {
    listTrabajos: [],
    idsPostulados: [],
  }

  componentDidMount() {
    const { getTrabajo, user: { id } } = this.props

    getTrabajo(id)
  }

  static getDerivedStateFromProps(prevProps, prevState) {
    if (!isEmpty(prevProps.listTrabajos) && prevProps.listTrabajos !== prevState.listTrabajos) {
      const idsPostulados = []
      prevProps.listTrabajos.forEach(({ id, postulado }) => {
        if (postulado) {
          idsPostulados.push(id)
        }
      })

      return {
        listTrabajos: prevProps.listTrabajos,
        idsPostulados,
      }
    }

    return null
  }

  componentDidUpdate(prevProps) {
    const { successPostulacion } = this.props
    if (successPostulacion && successPostulacion !== prevProps.successPostulacion) {
      this.updateListaTrabajo(successPostulacion)
    }
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  updateListaTrabajo = ({ idTrabajo }) => {
    const { listTrabajos, idsPostulados } = this.state
    const { successTrabajo } = this.props


    const nuevaListaTrabajo = listTrabajos.map((trabajo) => {
      if (trabajo.id === idTrabajo) {
        trabajo.postulado = true
        return trabajo
      }
      return trabajo
    })

    idsPostulados.push(idTrabajo)
    successTrabajo(nuevaListaTrabajo)
    this.setState({
      idsPostulados,
    })
  }

  handleClickPostular = (id) => {
    const { postPostulacion, user } = this.props

    const request = {
      idUsuario: user.id,
      idTrabajo: id,
    }

    postPostulacion(request)
  }

  handleMostrarTrabajo = (id) => {
    const { history } = this.props

    history.push(`/job/show/${id}`)
  }

  validarFechaFormato = (fecha) => {
    const s = moment().diff(fecha, 's')
    const m = moment().diff(fecha, 'm')
    const h = moment().diff(fecha, 'h')
    let value
    if (s < 60) {
      value = `hace ${s} seg`
    } else if (m < 60) {
      value = `hace ${m} min`
    } else if (h < 24) {
      value = `hace ${h} hora`
    }

    return value
  }

  render() {
    const { listTrabajos, idsPostulados } = this.state
    const { loadingListTrabajos, loadingSuccessPostulacion, user } = this.props

    let optionTrabajos = []
    if (!isEmpty(listTrabajos)) {
      listTrabajos.sort((f1, f2) => {
        const a = new Date(f1.fecha);
        const b = new Date(f2.fecha);
        // eslint-disable-next-line no-nested-ternary
        return a > b ? -1 : a < b ? 1 : 0;
      })

      optionTrabajos = listTrabajos.map(({ titulo, descripcion, id, postulado, fecha }, index) => (
        <div key={index} className="lista-trabajos">
          <div className="trabajo-header">
            <div className="trabajo-titulo" onClick={() => this.handleMostrarTrabajo(id)}>{titulo}</div>
            <div>
              <span>{this.validarFechaFormato(fecha)}</span>
              {/* <span>{moment.duration(moment(moment()).diff(fecha)).hours()}</span> */}
              <Button
                className="trabajo-boton"
                disabled={idsPostulados.includes(id) || user.idRol !== 3}
                loading={loadingSuccessPostulacion}
                size="mini"
                onClick={() => this.handleClickPostular(id)}
              >
                <Icon name="check" />{postulado ? 'Postulado' : 'Postular'}
              </Button>
            </div>
          </div>
          <div className="trabajo-descripcion">
            {`- ${descripcion}`}
          </div>
        </div>
      ))
    }

    return (
      <Fragment>
        <div className="content-view">
          <Header className="content-title" as="h2">Empleos</Header>
          <Segment
            basic
          >
            <Dimmer active={loadingListTrabajos} inverted>
              <Loader>Loading ...</Loader>
            </Dimmer>
            <Card className="card-content-custom">
              <Card.Content>
                {optionTrabajos}
              </Card.Content>
            </Card>
          </Segment>
          <div className="back" />
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  listTrabajos: state.trabajo.list.data,
  loadingListTrabajos: state.trabajo.list.loading,

  successPostulacion: state.postulacion.create.created,
  loadingSuccessPostulacion: state.postulacion.create.loading,

  listPostulaciones: state.postulacion.list.data,

  user: state.auth.show.retrieved,
})

const mapDispatchToProps = dispatch => ({
  getTrabajo: id => dispatch(getTrabajo(id)),
  postPostulacion: obj => dispatch(postPostulacion(obj)),
  getPostulaciones: obj => dispatch(getPostulaciones(obj)),
  successTrabajo: obj => dispatch(successTrabajo(obj)),
  reset: () => {
    dispatch(resetTrabajo())
    dispatch(resetPostulaciones())
  },
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Home)

export default withRouter(Main)

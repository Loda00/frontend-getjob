import React, { Component, Fragment } from 'react'
import { isEmpty } from 'lodash'
import { Card, Header, Segment, Dimmer, Loader, Form, Grid, Button, Icon } from 'semantic-ui-react'
import { JobButton } from 'components'
import { withRouter } from 'react-router-dom'
import { list as getArea } from 'actions/area/list'
import { create as createTrabajo } from 'actions/trabajo/create'
import { connect } from 'react-redux'

class Create extends Component {
  state = {
    titulo: '',
    descripcion: '',
    otros: '',
    requisitos: [''],
    beneficios: [''],
    area: '',
    listArea: [],
    errorTitulo: false,
    errorDescripcion: false,
    errorOtros: false,
    errorArea: false,
    // errorRequisitos: false,
    // errorBeneficios: false,
  }

  componentDidMount() {
    const { getArea } = this.props

    getArea()
  }

  static getDerivedStateFromProps(prevProps, prevState) {
    if (!isEmpty(prevProps.listArea) && prevProps.listArea !== prevState.listArea) {
      return {
        listArea: prevProps.listArea,
      }
    }

    return null
  }

  componentDidUpdate(prevProps) {
    const { successTrabajo, history } = this.props

    if (successTrabajo && prevProps.prevProps !== prevProps) {
      console.log('successTrabajo', successTrabajo)
      history.push('/job')
    }
  }

  handleInputChange = (e) => {
    e.preventDefault()

    const { name, value } = e.target

    this.setState({
      [name]: value,
    })
  }

  handleChangeCombo = (e, data) => {
    e.preventDefault()
    const { name, value } = data

    this.setState({
      [name]: value,
    })
  }

  handleInputChangeRequisito = (e, index) => {
    e.preventDefault()
    const { requisitos } = this.state
    const { value } = e.target

    const nuevoRequisito = requisitos.map((requisito, i) => {
      if (i === index) {
        return value
      }
      return requisito
    })

    this.setState({
      requisitos: nuevoRequisito,
    })
  }

  handleAddRequisito = () => {
    const { requisitos } = this.state;

    requisitos.push('')

    this.setState({
      requisitos,
    })
  }

  handleRemoveRequisito = (index) => {
    const { requisitos } = this.state;

    requisitos.splice(index, 1)

    this.setState({
      requisitos,
    })
  }

  handleAddBeneficio = () => {
    const { beneficios } = this.state;

    beneficios.push('')

    this.setState({
      beneficios,
    })
  }

  handleRemoveBeneficio = (index) => {
    const { beneficios } = this.state;

    beneficios.splice(index, 1)

    this.setState({
      beneficios,
    })
  }

  handleInputChangeBeneficio = (e, index) => {
    e.preventDefault()
    const { beneficios } = this.state
    const { value } = e.target

    const nuevoBeneficio = beneficios.map((beneficio, i) => {
      if (i === index) {
        return value
      }
      return beneficio
    })

    this.setState({
      beneficios: nuevoBeneficio,
    })
  }

  handleSave = () => {
    const {
      titulo,
      descripcion,
      otros,
      beneficios,
      requisitos,
      area,
    } = this.state

    const { user, createTrabajo } = this.props

    this.setState({
      errorArea: false,
      errorDescripcion: false,
      errorOtros: false,
      errorTitulo: false,
    })

    let isValid = true

    if (isEmpty(titulo)) {
      isValid = false

      this.setState({
        errorTitulo: true,
      })
    }

    if (isEmpty(descripcion)) {
      isValid = false

      this.setState({
        errorDescripcion: true,
      })
    }

    if (isEmpty(otros)) {
      isValid = false

      this.setState({
        errorOtros: true,
      })
    }

    if (isEmpty(beneficios) || (!isEmpty(beneficios) && isEmpty(beneficios[0]))) {
      isValid = false

      // this.setState({
      //   errorTitulo: true,
      // })
    }

    if (isEmpty(requisitos) || (!isEmpty(requisitos) && isEmpty(requisitos[0]))) {
      isValid = false

      // this.setState({
      //   errorTitulo: true,
      // })
    }
    console.log('areaarea', area)
    if (isEmpty(String(area))) {
      isValid = false

      this.setState({
        errorArea: true,
      })
    }

    if (!isValid) return

    const request = {
      titulo,
      descripcion,
      otros,
      // idEstado,
      idUsuario: user.id,
      requisitos: JSON.stringify(requisitos),
      beneficios: JSON.stringify(beneficios),
      idArea: area,
    }

    console.log('request', request)
    createTrabajo(request)
  }

  render() {
    const {
      titulo,
      descripcion,
      otros,
      requisitos,
      beneficios,
      listArea,
      errorTitulo,
      errorDescripcion,
      errorOtros,
      errorArea,
    } = this.state

    const { loadingListArea, loadingCreateTrabajo } = this.props

    let optionsArea = []
    if (!isEmpty(listArea)) {
      optionsArea = listArea.map(({ area, id }, i) => ({
        key: i,
        text: area,
        value: id,
      }))
    }

    return (
      <Fragment>
        <div className="content-view">
          <Header className="content-title" as="h2">Crear empleo</Header>
          <Segment
            basic
          >
            <Dimmer active={loadingCreateTrabajo} inverted>
              <Loader>Loading ...</Loader>
            </Dimmer>
            <Card className="card-content-custom">
              <Card.Content>
                <Grid>
                  <Grid.Row>
                    <Grid.Column>
                      <Form className="main-form fields inline field" loading={loadingCreateTrabajo} size="small">

                        <Form.Group inline>
                          <Form.Input
                            label="Título :"
                            name="titulo"
                            placeholder="Título"
                            value={titulo}
                            disabled={loadingCreateTrabajo}
                            onChange={this.handleInputChange}
                            error={errorTitulo}
                          />
                        </Form.Group>

                        <Form.Group inline>
                          <Form.TextArea
                            label="Descripción :"
                            name="descripcion"
                            placeholder="Descripción"
                            value={descripcion}
                            disabled={loadingCreateTrabajo}
                            onChange={this.handleInputChange}
                            error={errorDescripcion}
                          />
                        </Form.Group>

                        <Form.Group inline>
                          <Form.Input
                            label="Otros :"
                            name="otros"
                            placeholder="Otros"
                            value={otros}
                            disabled={loadingCreateTrabajo}
                            onChange={this.handleInputChange}
                            error={errorOtros}
                          />
                        </Form.Group>

                        <Form.Group grouped>
                          {requisitos.map((requisito, i) => (
                            <Form.Group inline key={i}>
                              <Form.Input
                                label={`Requisito #${i} :`}
                                name={`Requisito${i}`}
                                placeholder={`Requisito #${i}`}
                                value={requisito}
                                disabled={loadingCreateTrabajo}
                                onChange={e => this.handleInputChangeRequisito(e, i)}
                                // error={errorOtros}
                              />
                              <Button size="mini" color="red" onClick={() => this.handleRemoveRequisito(i)}>
                                <Icon name="delete" />
                              </Button>
                            </Form.Group>
                          ))}
                        </Form.Group>
                        <Button size="mini" color="green" onClick={this.handleAddRequisito}>
                          <Icon name="plus" />
                        </Button>

                        <Form.Group grouped>
                          {beneficios.map((beneficio, i) => (
                            <Form.Group inline key={i}>
                              <Form.Input
                                label={`Beneficio #${i} :`}
                                name={`Beneficio${i}`}
                                placeholder={`Beneficio #${i}`}
                                value={beneficio}
                                disabled={loadingCreateTrabajo}
                                onChange={e => this.handleInputChangeBeneficio(e, i)}
                                // error={errorOtros}
                              />
                              <Button size="mini" color="red" onClick={() => this.handleRemoveBeneficio(i)}>
                                <Icon name="delete" />
                              </Button>
                            </Form.Group>
                          ))}
                        </Form.Group>
                        <Button size="mini" color="green" onClick={this.handleAddBeneficio}>
                          <Icon name="plus" />
                        </Button>

                        <Form.Group inline>
                          <Form.Select
                            label="Area :"
                            name="area"
                            placeholder="Area"
                            clearable
                            options={optionsArea}
                            loading={loadingListArea || loadingCreateTrabajo}
                            disabled={loadingListArea || loadingCreateTrabajo}
                            onChange={(e, data) => this.handleChangeCombo(e, data)}
                            error={errorArea}
                          />
                        </Form.Group>
                      </Form>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Card.Content>
            </Card>
            <JobButton icon="check" loading={loadingCreateTrabajo} onClick={this.handleSave}>
              Crear
            </JobButton>
          </Segment>
          <div className="back" />
        </div>
      </Fragment>

    )
  }
}

const mapStateToProps = state => ({
  listArea: state.area.list.data,
  loadingListArea: state.area.list.loading,

  successTrabajo: state.trabajo.create.created,
  loadingCreateTrabajo: state.trabajo.create.loading,

  user: state.auth.show.retrieved,
})

const mapDispatchToProps = dispatch => ({
  getArea: () => dispatch(getArea()),
  createTrabajo: obj => dispatch(createTrabajo(obj)),
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Create)

export default withRouter(Main)

import React, { Fragment } from 'react'
import { Route, Switch } from 'react-router-dom'

import NotFound from '../404'
import Create from './Create'
import Home from './Home'
import Show from './Show'

const Job = () => (
  <Fragment>
    <Switch>
      <Route exact path="/job" component={Home} />
      <Route exact path="/job/create" component={Create} />
      <Route exact path="/job/show/:id" component={Show} />
      <Route component={NotFound} />
    </Switch>
  </Fragment>
)

export default Job

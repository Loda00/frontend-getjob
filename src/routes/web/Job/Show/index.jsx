import React, { Fragment, Component } from 'react'
import { Card, Header, Segment, Dimmer, Loader, Container, List, Button, Icon, Rating } from 'semantic-ui-react'
import { isEmpty } from 'lodash'
import { withRouter } from 'react-router-dom'
import { list as getTrabajo, reset as resetTrabajo } from 'actions/trabajo/list'
import { list as getPostulaciones, reset as resetPostulaciones } from 'actions/postulaciones/list'
import { create as postPostulacion } from 'actions/postulaciones/create'
import { success as successTrabajo } from 'actions/trabajo/create'
import { connect } from 'react-redux'

class Show extends Component {
  state = {
    listTrabajos: [],
    idsPostulados: [],
    trabajoSeleccionado: [],
    requisitos: [],
    beneficios: [],
  }

  componentDidMount() {
    const {
      match: { params: { id } },
      listTrabajos,
      getTrabajo,
      user,
    } = this.props


    let trabajo = []

    if (id && !isEmpty(listTrabajos)) {
      // console.log('listTrabajos', listTrabajos)
      trabajo = listTrabajos.find(trabajo => trabajo.id === Number(id))
      this.setState({
        trabajoSeleccionado: trabajo,
        requisitos: JSON.parse(trabajo.requisitos),
        beneficios: JSON.parse(trabajo.beneficios),
      })
    } else {
      getTrabajo(user.id)
    }
  }

  static getDerivedStateFromProps(prevProps, prevState) {
    if (!isEmpty(prevProps.listTrabajos) && prevProps.listTrabajos !== prevState.listTrabajos) {
      const trabajoSeleccionado = []
      prevProps.listTrabajos.forEach((trabajo) => {
        if (trabajo.id === Number(prevProps.match.params.id)) {
          trabajoSeleccionado.push(trabajo)
        }
      })
      return {
        trabajoSeleccionado: trabajoSeleccionado[0],
        requisitos: JSON.parse(trabajoSeleccionado[0].requisitos),
        beneficios: JSON.parse(trabajoSeleccionado[0].beneficios),
      }
    }

    return null
  }

  componentDidUpdate(prevProps) {
    const { successPostulacion } = this.props
    if (successPostulacion && successPostulacion !== prevProps.successPostulacion) {
      this.updateListaTrabajo(successPostulacion)
    }
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  updateListaTrabajo = ({ idTrabajo }) => {
    const { listTrabajos, idsPostulados } = this.state
    const { successTrabajo } = this.props

    const nuevaListaTrabajo = listTrabajos.map((trabajo) => {
      if (trabajo.id === idTrabajo) {
        trabajo.postulado = true
        return trabajo
      }
      return trabajo
    })

    idsPostulados.push(idTrabajo)
    successTrabajo(nuevaListaTrabajo)
    this.setState({
      idsPostulados,
    })
  }

  handleClickPostular = (id) => {
    const { postPostulacion, user } = this.props

    const request = {
      idUsuario: user.id,
      idTrabajo: id,
    }

    postPostulacion(request)
  }

  render() {
    const {
      trabajoSeleccionado: {
        otros,
        titulo,
        descripcion,
        id,
        postulado,
        idRol,
        nombres,
      },
      requisitos,
      beneficios,
    } = this.state
    const { loadingListTrabajos } = this.props

    let optionsBeneficios = []
    if (!isEmpty(beneficios)) {
      optionsBeneficios = beneficios.map(beneficio => (
        <List bulleted>
          <List.Item>{beneficio}</List.Item>
        </List>
      ))
    }

    let optionsRequisitos = []
    if (!isEmpty(requisitos)) {
      optionsRequisitos = requisitos.map(requisito => (
        <List bulleted>
          <List.Item>{requisito}</List.Item>
        </List>
      ))
    }

    return (
      <Fragment>
        <div className="content-view">
          <Header className="content-title" as="h2">Empleos</Header>
          <Segment
            basic
          >
            <Dimmer active={loadingListTrabajos} inverted>
              <Loader>Loading ...</Loader>
            </Dimmer>
            <Card className="card-content-custom">
              <Card.Content>
                <Container>
                  <Button
                    className="trabajo-boton"
                    disabled={postulado || idRol !== 3}
                    // loading={loadingSuccessPostulacion}
                    size="mini"
                    onClick={() => this.handleClickPostular(id)}
                  >
                    <Icon name="check" />{postulado ? 'Postulado' : 'Postular'}
                  </Button>
                  <Header as="h2">{titulo}</Header>
                  <Header as="h3">{`${nombres} `}<Rating icon="star" defaultRating={3} maxRating={5} /></Header>
                  <p>
                    {descripcion}
                  </p>
                  <Header as="h4">Requitos :</Header>
                  {optionsBeneficios}
                  <Header as="h4">Beneficios :</Header>
                  {optionsRequisitos}
                  <Header as="h4">Otros :</Header>
                  {otros}
                  <p>
                    {otros}
                  </p>
                </Container>
              </Card.Content>
            </Card>
          </Segment>
          <div className="back" />
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  listTrabajos: state.trabajo.list.data,
  loadingListTrabajos: state.trabajo.list.loading,

  successPostulacion: state.postulacion.create.created,
  loadingSuccessPostulacion: state.postulacion.create.loading,

  listPostulaciones: state.postulacion.list.data,

  user: state.auth.show.retrieved,
})

const mapDispatchToProps = dispatch => ({
  getTrabajo: id => dispatch(getTrabajo(id)),
  postPostulacion: obj => dispatch(postPostulacion(obj)),
  getPostulaciones: obj => dispatch(getPostulaciones(obj)),
  successTrabajo: obj => dispatch(successTrabajo(obj)),
  reset: () => {
    dispatch(resetTrabajo())
    dispatch(resetPostulaciones())
  },
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Show)

export default withRouter(Main)

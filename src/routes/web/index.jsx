import React, { Component, Fragment } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import { connect } from 'react-redux'
import { Container } from 'layout'

import Home from './Home'
import Nuevo from './Nuevo'
import Job from './Job'
import Postulaciones from './Postulaciones'
import Usuario from './Usuario'
import NuevoUsuario from '../login/Create'
import NotFound from './404'

class Web extends Component {
  componentDidMount() {

  }

  render() {
    const { user } = this.props

    if (user) {
      return (
        <Fragment>
          <Container>
            <Switch>
              <Route
                exact
                path="/"
                render={() => (
                  <Redirect
                    to={{
                      pathname: user ? '/job' : '/login',
                    }}
                  />
                )}
              />
              <Route path="/index" component={Home} />
              <Route path="/job" component={Job} />
              <Route path="/nuevo/nuevo-hdb" component={Nuevo} />
              <Route path="/postulacion/mis-postulaciones" component={Postulaciones} />
              <Route path="/perfil/editar" component={Usuario} />
              <Route component={NotFound} />
            </Switch>
          </Container>
          <ToastContainer
            position="bottom-right"
            hideProgressBar
            draggable={false}
            toastClassName="custom-toast-container"
            bodyClassName="custom-toast-body"
          />
        </Fragment>
      )
    }

    if (!user) {
      return (
        <Fragment>
          <Route
            exact
            path="/"
            render={props => (
              <Redirect
                to={{
                  pathname: '/login',
                  state: {
                    from: props.location,
                  },
                }}
              />
            )}
          />
          <Route path="/registro" component={NuevoUsuario} />
          <ToastContainer
            position="bottom-right"
            hideProgressBar
            draggable={false}
            toastClassName="custom-toast-container"
            bodyClassName="custom-toast-body"
          />
        </Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  user: state.auth.show.retrieved,
  // user: state,
})

const Main = connect(mapStateToProps)(Web)

export default Main

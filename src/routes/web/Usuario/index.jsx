import React, { Fragment } from 'react'
import { Route, Switch } from 'react-router-dom'

import NotFound from '../404'
// import Create from './Create'
import Home from './Home'

const Postulaciones = () => (
  <Fragment>
    <Switch>
      <Route exact path="/perfil/editar" component={Home} />
      {/* <Route exact path="/postulacion" component={Create} /> */}
      <Route component={NotFound} />
    </Switch>
  </Fragment>
)

export default Postulaciones

import React, { Fragment, Component } from 'react'
import { Card, Header, Segment, Dimmer, Loader, Form } from 'semantic-ui-react'
import { isEmpty } from 'lodash'
import { list as getCentroEstudios, reset as resetCentroEstudios } from 'actions/centroEstudios/list'
import { list as getDepartamento, reset as resetDepartamento } from 'actions/departamento/list'
import { list as getDistrito, reset as resetDistrito } from 'actions/distrito/list'
import { list as getPais, reset as resetPais } from 'actions/pais/list'
import { list as getProvincia, reset as resetProvincia } from 'actions/provincia/list'
import { connect } from 'react-redux'

class Home extends Component {
  state = {
    listCentroEstudios: [],
    listPais: [],
    listDepartamento: [],
    listProvincia: [],
    listDistrito: [],
  }

  componentDidMount() {
    const { getCentroEstudios, getDepartamento, getDistrito, getPais, getProvincia } = this.props

    getCentroEstudios()
    getDepartamento()
    getDistrito()
    getPais()
    getProvincia()
  }

  static getDerivedStateFromProps(prevProps, prevState) {
    if (!isEmpty(prevProps.listCentroEstudios)
      && prevProps.listCentroEstudios !== prevState.listCentroEstudios) {
      return {
        listCentroEstudios: prevProps.listCentroEstudios,
      }
    }

    if (!isEmpty(prevProps.listPais) && prevProps.listPais !== prevState.listPais) {
      return {
        listPais: prevProps.listPais,
      }
    }

    if (!isEmpty(prevProps.listDepartamento)
      && prevProps.listDepartamento !== prevState.listDepartamento) {
      return {
        listDepartamento: prevProps.listDepartamento,
      }
    }

    if (!isEmpty(prevProps.listProvincia) && prevProps.listProvincia !== prevState.listProvincia) {
      return {
        listProvincia: prevProps.listProvincia,
      }
    }

    if (!isEmpty(prevProps.listDistrito) && prevProps.listDistrito !== prevState.listDistrito) {
      return {
        listDistrito: prevProps.listDistrito,
      }
    }

    return null
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  render() {
    // eslint-disable-next-line no-unused-vars
    const {
      listCentroEstudios,
      listPais,
      listDepartamento,
      listDistrito,
      listProvincia,
    } = this.state

    const {
      user: {
        apellidos,
        correo,
        idCentroEstudios,
        idDepartamento,
        idDistrito,
        idPais,
        idProvincia,
        presentacion,
        nombres,
        numero,
      },
    } = this.props

    let optionsCentroEstudios = []
    if (!isEmpty(listCentroEstudios)) {
      optionsCentroEstudios = listCentroEstudios.map(({ nombreCentroE, idCentroE }, i) => ({
        key: i,
        text: nombreCentroE,
        value: idCentroE,
      }))
    }

    let optionsPais = []
    if (!isEmpty(listPais)) {
      optionsPais = listPais.map(({ id, pais }, i) => ({
        key: i,
        text: pais,
        value: id,
      }))
    }

    let optionsDepartamento = []
    if (!isEmpty(listDepartamento)) {
      optionsDepartamento = listDepartamento.map(({ departamento, id }, i) => ({
        key: i,
        text: departamento,
        value: id,
      }))
    }

    let optionsDistrito = []
    if (!isEmpty(listDistrito)) {
      optionsDistrito = listDistrito.map(({ distrito, id }, i) => ({
        key: i,
        text: distrito,
        value: id,
      }))
    }

    let optionsProvincia = []
    if (!isEmpty(listProvincia)) {
      optionsProvincia = listProvincia.map(({ provincia, id }, i) => ({
        key: i,
        text: provincia,
        value: id,
      }))
    }

    return (
      <Fragment>
        <div className="content-view">
          <Header className="content-title" as="h2">Mi perfil</Header>
          <Segment
            basic
          >
            <Dimmer active={false} inverted>
              <Loader>Loading ...</Loader>
            </Dimmer>
            <Card className="card-content-custom">
              <Card.Content>
                <Form className="main-form fields inline field" loading={false} size="small">

                  <Form.Group inline>
                    <Form.Field>
                      <label>Nombres :</label>
                      <h5 className="informative-field">{nombres}</h5>
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <label>Apellidos :</label>
                      <h5 className="informative-field">{apellidos}</h5>
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <label>Correo :</label>
                      <h5 className="informative-field">{correo}</h5>
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <label>Teléfono :</label>
                      <h5 className="informative-field">{numero}</h5>
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <Form.Select
                        label="Estudios :"
                        name="estudios"
                        placeholder="Estudios"
                        options={optionsCentroEstudios}
                        loading={false}
                        disabled
                        value={idCentroEstudios}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <Form.Select
                        label="Pais :"
                        name="pais"
                        placeholder="Pais"
                        options={optionsPais}
                        loading={false}
                        disabled
                        value={idPais}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <Form.Select
                        label="Departamento :"
                        name="departamento"
                        placeholder="Departamento"
                        options={optionsDepartamento}
                        loading={false}
                        disabled
                        value={idDepartamento}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <Form.Select
                        label="Provincia :"
                        name="Provincia"
                        placeholder="Provincia"
                        options={optionsProvincia}
                        loading={false}
                        disabled
                        value={idProvincia}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <Form.Select
                        label="Distrito :"
                        name="distrito"
                        placeholder="Distrito"
                        options={optionsDistrito}
                        loading={false}
                        disabled
                        value={idDistrito}
                      />
                    </Form.Field>
                  </Form.Group>

                  <Form.Group inline>
                    <Form.Field>
                      <Form.TextArea
                        label="Presentación :"
                        name="presentación"
                        placeholder="Presentación"
                        loading={false}
                        disabled
                        value={presentacion}
                      />
                    </Form.Field>
                  </Form.Group>
                </Form>
              </Card.Content>
            </Card>
          </Segment>
          <div className="back" />
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  listCentroEstudios: state.centroEstudios.list.data,
  loadingListCentroEstudios: state.centroEstudios.list.loading,

  listPais: state.pais.list.data,
  loadingListPais: state.pais.list.loading,

  listProvincia: state.provincia.list.data,
  loadingListProvincia: state.provincia.list.loading,

  listDistrito: state.distrito.list.data,
  loadingListDistrito: state.distrito.list.loading,

  listDepartamento: state.departamento.list.data,
  loadingListDepartamento: state.departamento.list.loading,

  user: state.auth.show.retrieved,
})

const mapDispatchToProps = dispatch => ({
  getCentroEstudios: () => dispatch(getCentroEstudios()),
  getDepartamento: () => dispatch(getDepartamento()),
  getDistrito: () => dispatch(getDistrito()),
  getPais: () => dispatch(getPais()),
  getProvincia: () => dispatch(getProvincia()),
  reset: () => {
    dispatch(resetCentroEstudios())
    dispatch(resetDepartamento())
    dispatch(resetDistrito())
    dispatch(resetPais())
    dispatch(resetProvincia())
  },
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Home)

export default Main

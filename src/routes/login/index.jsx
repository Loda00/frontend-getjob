import React from 'react'
import { connect } from 'react-redux'
import { Switch } from 'react-router-dom';
import Login from './login'
import ButtonFb from './Create'

const Route = () => (
  <Switch>
    <Route exact path="/login" component={Login} />
    <Route exact path="/registro" component={ButtonFb} />
  </Switch>
)

const mapStateToProps = state => ({
  test: state,
})

const Main = connect(mapStateToProps)(Login)

export default Main

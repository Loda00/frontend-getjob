import React, { Fragment, Component } from 'react'
import { Card, Header, Segment, Dimmer, Loader, Form } from 'semantic-ui-react'
import { isEmpty } from 'lodash'
import { JobButton } from 'components'
import { withRouter } from 'react-router-dom'
import { list as getCentroEstudios, reset as resetCentroEstudios } from 'actions/centroEstudios/list'
import { list as getDepartamento, reset as resetDepartamento } from 'actions/departamento/list'
import { list as getDistrito, reset as resetDistrito } from 'actions/distrito/list'
import { list as getPais, reset as resetPais } from 'actions/pais/list'
import { list as getProvincia, reset as resetProvincia } from 'actions/provincia/list'
import { list as getRol, reset as resetRol } from 'actions/rol/list'
import { create as createUser } from 'actions/users/create'
import { retrieve } from 'actions/auth/auth'
import { connect } from 'react-redux'

class Home extends Component {
  state = {
    listCentroEstudios: [],
    listPais: [],
    listDepartamento: [],
    listProvincia: [],
    listDistrito: [],
    listRol: [],
    nombres: '',
    apellidos: '',
    contrasenia: '',
    correo: '',
    // numero: '',
    // idPais: '',
    // idDepartamento: '',
    // idProvincia: '',
    // idDistrito: '',
    // idCentroEstudios: '',
    idRol: 3,
    // presentacion: '',
    errorNombres: false,
    errorApellidos: false,
    errorCorreo: false,
    // errorNumero: false,
    errorContrasenia: false,
    // errorIdPais: false,
    // errorIdDepartamento: false,
    // errorIdProvincia: false,
    errorIdRol: false,
    // errorIdDistrito: false,
    // errorIdCentroEstudios: false,
    // errorPresentacion: false,
  }

  componentDidMount() {
    const {
      getCentroEstudios,
      getDepartamento,
      getDistrito,
      getPais,
      getProvincia,
      getRol,
    } = this.props

    getCentroEstudios()
    getDepartamento()
    getDistrito()
    getPais()
    getProvincia()
    getRol()
  }

  static getDerivedStateFromProps(prevProps, prevState) {
    if (!isEmpty(prevProps.listCentroEstudios)
      && prevProps.listCentroEstudios !== prevState.listCentroEstudios) {
      return {
        listCentroEstudios: prevProps.listCentroEstudios,
      }
    }

    if (!isEmpty(prevProps.listPais) && prevProps.listPais !== prevState.listPais) {
      return {
        listPais: prevProps.listPais,
      }
    }

    if (!isEmpty(prevProps.listDepartamento)
      && prevProps.listDepartamento !== prevState.listDepartamento) {
      return {
        listDepartamento: prevProps.listDepartamento,
      }
    }

    if (!isEmpty(prevProps.listProvincia) && prevProps.listProvincia !== prevState.listProvincia) {
      return {
        listProvincia: prevProps.listProvincia,
      }
    }

    if (!isEmpty(prevProps.listDistrito) && prevProps.listDistrito !== prevState.listDistrito) {
      return {
        listDistrito: prevProps.listDistrito,
      }
    }

    if (!isEmpty(prevProps.listRol) && prevProps.listRol !== prevState.listRol) {
      return {
        listRol: prevProps.listRol,
      }
    }

    return null
  }

  componentDidUpdate(prevProps) {
    const { successCreateUser, postUser, history, errorCreateUser } = this.props
    console.log('errorCreateUser', errorCreateUser)
    if (successCreateUser && !errorCreateUser
      && prevProps.successCreateUser !== successCreateUser) {
      const request = {
        correo: successCreateUser.correo,
        contrasenia: successCreateUser.contrasenia,
      }
      postUser(request)
      history.push('/job')
    }
  }

  componentWillUnmount() {
    const { reset } = this.props
    reset()
  }

  handleInputChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target

    this.setState({
      [name]: value,
    })
  }

  handleChangeCombo = (e, data) => {
    e.preventDefault()
    const { name, value } = data

    this.setState({
      [name]: value,
    })
  }

  handleSave = () => {
    const {
      apellidos,
      correo,
      contrasenia,
      // idCentroEstudios,
      // idDepartamento,
      // idDistrito,
      // idPais,
      // idProvincia,
      // numero,
      idRol,
      // presentacion,
      nombres,
    } = this.state

    const { createUser } = this.props

    this.setState({
      errorNombres: false,
      errorApellidos: false,
      errorCorreo: false,
      errorIdRol: false,
      errorContrasenia: false,
      // errorIdPais: false,
      // errorIdDepartamento: false,
      // errorIdProvincia: false,
      // errorIdDistrito: false,
      // errorIdCentroEstudios: false,
      // errorPresentacion: false,
    })

    let isValid = true

    if (isEmpty(nombres)) {
      isValid = false

      this.setState({
        errorNombres: true,
      })
    }

    if (idRol === 3 && isEmpty(apellidos)) {
      isValid = false

      this.setState({
        errorApellidos: true,
      })
    }

    if (isEmpty(correo)) {
      isValid = false

      this.setState({
        errorCorreo: true,
      })
    }

    if (isEmpty(contrasenia)) {
      isValid = false

      this.setState({
        errorContrasenia: true,
      })
    }

    if (!Number(idRol)) {
      isValid = false

      this.setState({
        errorIdRol: true,
      })
    }

    // if (isEmpty(idCentroEstudios)) {
    //   isValid = false

    //   this.setState({
    //     errorIdCentroEstudios: true,
    //   })
    // }

    // if (isEmpty(idPais)) {
    //   isValid = false

    //   this.setState({
    //     errorIdPais: true,
    //   })
    // }

    // if (isEmpty(idDepartamento)) {
    //   isValid = false

    //   this.setState({
    //     errorIdDepartamento: true,
    //   })
    // }

    // if (isEmpty(idProvincia)) {
    //   isValid = false

    //   this.setState({
    //     errorIdProvincia: true,
    //   })
    // }

    // if (isEmpty(idDistrito)) {
    //   isValid = false

    //   this.setState({
    //     errorIdDistrito: true,
    //   })
    // }

    // if (isEmpty(idRol)) {
    //   isValid = false

    //   this.setState({
    //     errorIdRol: true,
    //   })
    // }

    // if (isEmpty(presentacion)) {
    //   isValid = false

    //   this.setState({
    //     errorPresentacion: true,
    //   })
    // }
    const request = {
      contrasenia,
      nombres,
      apellidos: idRol === 2 ? '-' : apellidos,
      correo,
      idRol,
    }

    console.log('request', request)

    if (!isValid) return

    createUser(request)
  }

  render() {
    const {
      // listCentroEstudios,
      // listPais,
      // listDepartamento,
      // listDistrito,
      // listProvincia,
      listRol,
      apellidos,
      correo,
      // idCentroEstudios,
      // idDepartamento,
      // idDistrito,
      // idPais,
      // idProvincia,
      idRol,
      // presentacion,
      contrasenia,
      nombres,
      // numero,
      // errorPresentacion,
      errorNombres,
      errorApellidos,
      errorCorreo,
      // errorNumero,
      // errorIdPais,
      // errorIdDepartamento,
      // errorIdProvincia,
      // errorIdDistrito,
      errorIdRol,
      // errorIdCentroEstudios,
      errorContrasenia,
    } = this.state

    // const {
    //   loadingListCentroEstudios,
    //   loadingListPais,
    //   loadingListProvincia,
    //   loadingListDistrito,
    //   loadingListDepartamento,
    // } = this.props
    const { loadingListRol, loadingCreateUser } = this.props

    // let optionsCentroEstudios = []
    // if (!isEmpty(listCentroEstudios)) {
    //   optionsCentroEstudios = listCentroEstudios.map(({ nombreCentroE, idCentroE }, i) => ({
    //     key: i,
    //     text: nombreCentroE,
    //     value: idCentroE,
    //   }))
    // }

    // let optionsPais = []
    // if (!isEmpty(listPais)) {
    //   optionsPais = listPais.map(({ id, pais }, i) => ({
    //     key: i,
    //     text: pais,
    //     value: id,
    //   }))
    // }

    // let optionsDepartamento = []
    // if (!isEmpty(listDepartamento)) {
    //   optionsDepartamento = listDepartamento.map(({ departamento, id }, i) => ({
    //     key: i,
    //     text: departamento,
    //     value: id,
    //   }))
    // }

    // let optionsDistrito = []
    // if (!isEmpty(listDistrito)) {
    //   optionsDistrito = listDistrito.map(({ distrito, id }, i) => ({
    //     key: i,
    //     text: distrito,
    //     value: id,
    //   }))
    // }

    // let optionsProvincia = []
    // if (!isEmpty(listProvincia)) {
    //   optionsProvincia = listProvincia.map(({ provincia, id }, i) => ({
    //     key: i,
    //     text: provincia,
    //     value: id,
    //   }))
    // }

    let optionsRol = []
    if (!isEmpty(listRol)) {
      optionsRol = listRol.map(({ rol, id }, i) => ({
        key: i,
        text: rol,
        value: id,
      }))
    }

    return (
      <Fragment>
        <div
          className="img-fondo"
          style={{
            overflow: 'hidden',
            overflowY: 'scroll',
          }}
        >
          <div className="content-view">
            <Header className="content-title" as="h2">Crear usuario</Header>
            <Segment
              basic
            >
              <Dimmer active={false} inverted>
                <Loader>Loading ...</Loader>
              </Dimmer>
              <Card className="card-content-custom">
                <Card.Content>
                  <Form className="main-form fields inline field" loading={false} size="small">

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="Rol :"
                          name="idRol"
                          placeholder="Rol"
                          options={optionsRol}
                          loading={loadingListRol || loadingCreateUser}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          disabled={loadingCreateUser}
                          error={errorIdRol}
                          value={idRol}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Input
                          label={idRol === 3 ? 'Nombres :' : 'Empresa :'}
                          name="nombres"
                          placeholder={idRol === 3 ? 'Nombres :' : 'Empresa :'}
                          value={nombres}
                          disabled={loadingCreateUser}
                          loading={loadingCreateUser}
                          onChange={this.handleInputChange}
                          error={errorNombres}
                        />
                      </Form.Field>
                    </Form.Group>

                    {idRol === 3 && (
                      <Form.Group inline>
                        <Form.Field>
                          <Form.Input
                            label="Apellidos :"
                            name="apellidos"
                            placeholder="Apellidos"
                            value={apellidos}
                            disabled={loadingCreateUser}
                            loading={loadingCreateUser}
                            onChange={this.handleInputChange}
                            error={errorApellidos}
                          />
                        </Form.Field>
                      </Form.Group>
                    )}

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Input
                          label="Correo :"
                          name="correo"
                          placeholder="Correo"
                          value={correo}
                          disabled={loadingCreateUser}
                          loading={loadingCreateUser}
                          onChange={this.handleInputChange}
                          error={errorCorreo}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Input
                          label="Contraseña :"
                          name="contrasenia"
                          placeholder="Contraseña"
                          value={contrasenia}
                          disabled={loadingCreateUser}
                          loading={loadingCreateUser}
                          onChange={this.handleInputChange}
                          error={errorContrasenia}
                        />
                      </Form.Field>
                    </Form.Group>

                    {/* <Form.Group inline>
                      <Form.Field>
                        <Form.Input
                          label="Teléfono :"
                          name="numero"
                          placeholder="Teléfono"
                          value={numero}
                          disabled={false}
                          onChange={this.handleInputChange}
                          error={errorNumero}
                        />
                      </Form.Field>
                    </Form.Group> */}

                    {/* <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="Estudios :"
                          name="idCentroEstudios"
                          placeholder="Estudios"
                          options={optionsCentroEstudios}
                          loading={loadingListCentroEstudios}
                          disabled={false}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          value={idCentroEstudios}
                          error={errorIdCentroEstudios}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="País :"
                          name="idPais"
                          placeholder="País"
                          options={optionsPais}
                          loading={loadingListPais}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          disabled={false}
                          value={idPais}
                          error={errorIdPais}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="Departamento :"
                          name="idDepartamento"
                          placeholder="Departamento"
                          options={optionsDepartamento}
                          loading={loadingListDepartamento}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          disabled={false}
                          value={idDepartamento}
                          error={errorIdDepartamento}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="Provincia :"
                          name="idProvincia"
                          placeholder="Provincia"
                          options={optionsProvincia}
                          loading={loadingListProvincia}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          disabled={false}
                          value={idProvincia}
                          error={errorIdProvincia}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="Distrito :"
                          name="idDistrito"
                          placeholder="Distrito"
                          options={optionsDistrito}
                          loading={loadingListDistrito}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          disabled={false}
                          value={idDistrito}
                          error={errorIdDistrito}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.Select
                          label="Rol :"
                          name="idRol"
                          placeholder="Rol"
                          options={optionsRol}
                          loading={loadingListDistrito}
                          onChange={(e, data) => this.handleChangeCombo(e, data)}
                          disabled={false}
                          error={errorIdRol}
                          value={idRol}
                        />
                      </Form.Field>
                    </Form.Group>

                    <Form.Group inline>
                      <Form.Field>
                        <Form.TextArea
                          label="Presentación :"
                          name="presentación"
                          placeholder="Presentación"
                          loading={false}
                          disabled={false}
                          onChange={this.handleInputChange}
                          value={presentacion}
                          error={errorPresentacion}
                        />
                      </Form.Field>
                    </Form.Group> */}
                  </Form>
                </Card.Content>
              </Card>
              <JobButton
                icon="check"
                loading={loadingCreateUser}
                onClick={this.handleSave}
                style={{
                  right: '120px',
                  bottom: '150px',
                }}
              >
                Crear
              </JobButton>
            </Segment>
            <div className="back" />
          </div>
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  listCentroEstudios: state.centroEstudios.list.data,
  loadingListCentroEstudios: state.centroEstudios.list.loading,

  listPais: state.pais.list.data,
  loadingListPais: state.pais.list.loading,

  listProvincia: state.provincia.list.data,
  loadingListProvincia: state.provincia.list.loading,

  listDistrito: state.distrito.list.data,
  loadingListDistrito: state.distrito.list.loading,

  listDepartamento: state.departamento.list.data,
  loadingListDepartamento: state.departamento.list.loading,

  listRol: state.rol.list.data,
  loadingListRol: state.rol.list.loading,

  user: state.auth.show.retrieved,
  successCreateUser: state.users.create.created,
  loadingCreateUser: state.users.create.loading,
  errorCreateUser: state.users.create.error,
})

const mapDispatchToProps = dispatch => ({
  postUser: obj => dispatch(retrieve(obj)),
  createUser: obj => dispatch(createUser(obj)),
  getCentroEstudios: () => dispatch(getCentroEstudios()),
  getDepartamento: () => dispatch(getDepartamento()),
  getDistrito: () => dispatch(getDistrito()),
  getPais: () => dispatch(getPais()),
  getProvincia: () => dispatch(getProvincia()),
  getRol: () => dispatch(getRol()),
  reset: () => {
    dispatch(resetCentroEstudios())
    dispatch(resetDepartamento())
    dispatch(resetDistrito())
    dispatch(resetPais())
    dispatch(resetProvincia())
    dispatch(resetRol())
  },
})

const Main = connect(mapStateToProps, mapDispatchToProps)(Home)

export default withRouter(Main)
